﻿using System;

namespace LandBuilder.DomainModels
{
    [Serializable]
    public class Map
    {
        protected static Random Random = new Random();

        private Cell[,] _cells;

        public Map(int width, int height)
        {
            _cells = new Cell[width, height];
            Width = width;
            Height = height;

            for (var x = 0; x < width; x++)
            {
                for (var y = 0; y < height; y++)
                {
                    _cells[x, y] = new Cell(x, y);
                }
            }
        }

        public int Width { get; private set; }
        public int Height { get; private set; }

        public Cell this[int x, int y]
        {
            get
            {
                x = x % Width;
                if (x < 0)
                {
                    x += Width;
                }

                y = y % Height;
                if (y < 0)
                {
                    y += Height;
                }

                return _cells[x, y];
            }
        }

        public Cell RandomCell
        {
            get
            {
                var randomX = Random.Next(Width);
                var randomY = Random.Next(Height);
                return this[randomX, randomY];
            }
        }

        public double GetDistance(Cell from, Cell to)
        {
            int xd = GetDistanceByX(from, to);
            int yd = GetDistanceByY(from, to);

            return Math.Sqrt(xd * xd + yd * yd);
        }

        public void ForEachCell(Action<Cell> action)
        {
            for (var x = 0; x < Width; x++)
            {
                for (var y = 0; y < Height; y++)
                {
                    action(_cells[x, y]);
                }
                Console.Write(".");
            }
            Console.WriteLine();
        }

        public int GetDistanceByX(Cell from, Cell to)
        {
            return MinDistanceByOxis(from.X, to.X, Width);
        }

        public int GetDistanceByY(Cell from, Cell to)
        {
            return MinDistanceByOxis(from.Y, to.Y, Height);
        }

        private int MinDistanceByOxis(int from, int to, int edge)
        {
            int result = Math.Abs(from - to);
            return result < edge / 2
                ? result
                : edge - result;
        }
    }
}
