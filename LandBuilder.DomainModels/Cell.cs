﻿using System;

namespace LandBuilder.DomainModels
{
    [Serializable]
    public class Cell
    {
        public Cell(int x, int y, int value = 0)
        {
            X = x;
            Y = y;
            Height = value;
        }

        public int X { get; private set; }
        public int Y { get; private set; }
        public double Height { get; set; }
        public double CalculatedHeight { get; set; }

        public (int offsetByX, int offsetByY) GetCoordinateOffcets(double radians, int distance)
        {
            double cosine = Math.Cos(radians);
            int offsetByX = Convert.ToInt32(Math.Round(distance * cosine));

            double sinus = Math.Sin(radians);
            int offsetByY = Convert.ToInt32(Math.Round(distance * sinus));

            return (offsetByX, offsetByY);
        }

        public override bool Equals(object obj)
        {
            Cell anotherCell = obj as Cell;
            if (anotherCell == null)
            {
                return false;
            }

            return X == anotherCell.X
                && Y == anotherCell.Y;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode() ^ X.GetHashCode() ^ Y.GetHashCode() * 13;
        }

        public override string ToString()
        {
            return $"[{X}:{Y}] ({Height})";
        }
    }
}
