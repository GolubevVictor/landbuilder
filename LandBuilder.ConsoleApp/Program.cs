﻿using LandBuilder.ConsoleApp.Utilities;
using LandBuilder.DomainModels;
using LandBuilder.Landscape12Handler;
using LandBuilder.RandomHeightSmoothedMapGenerator;
using System;
using System.Diagnostics;

namespace LandBuilder.ConsoleApp
{
    class Program
    {
        private const int MapHeight = 500;
        private const int Smooth = 75
            ;

        static void Main(string[] args)
        {
            string fileNameBase = DateTime.Now.ToString("yyyyMMddHHmmss");

            var generator = new MapGenerator();
            var stopwatch = Stopwatch.StartNew();
            Map map = generator.Generate(MapHeight, Smooth);
            stopwatch.Stop();

            FileDataAccess.Save(map, $"original-{fileNameBase}.mg");

            //var map = FileDataAccess.Read("original-20191111022009.mg");
            var landscape = new Landscape();
            landscape.CreateRelief(map);

            Logger.Info($"{fileNameBase}. " +
                $"Map height: {MapHeight}. " +
                $"Smooth: {Smooth}. " +
                $"Finished in {Math.Round(stopwatch.Elapsed.TotalMinutes, 2)}). " +
                $"With 10 Mountains 6 continents. " +
                $"Land: 35%.");
            MapDrawing.Draw(map, fileNameBase);

            Console.Beep();
        }
    }
}
