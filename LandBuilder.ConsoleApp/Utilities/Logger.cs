﻿using log4net;
using System;
using System.IO;
using System.Reflection;
using System.Xml;

namespace LandBuilder.ConsoleApp.Utilities
{
    internal class Logger
    {
        static Logger()
        {
            XmlDocument log4netConfig = new XmlDocument();
            log4netConfig.Load(File.OpenRead("log4net.config"));
            var repo = log4net.LogManager.CreateRepository(Assembly.GetEntryAssembly(),
                       typeof(log4net.Repository.Hierarchy.Hierarchy));
            log4net.Config.XmlConfigurator.Configure(repo, log4netConfig["log4net"]);
        }

        private static ILog _logger;
        private static ILog PrivateLogger
        {
            get
            {
                if (_logger == null)
                {
                    _logger = LogManager.GetLogger(typeof(Program));
                }

                return _logger;
            }
        }

        internal static void Info(string message)
        {
            PrivateLogger.Info(message);
        }

        internal static void Error(string message)
        {
            PrivateLogger.Error(message);
        }

        internal static void Error(string message, Exception ex)
        {
            PrivateLogger.Error(message, ex);
        }
    }
}
