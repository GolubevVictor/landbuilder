﻿using LandBuilder.DomainModels;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace LandBuilder.ConsoleApp.Utilities
{
    internal class MapDrawing
    {
        private static Color[] colors;

        static MapDrawing()
        {
            colors = new Color[12];
            colors[0] = Color.FromArgb(34, 108, 39);
            colors[1] = Color.FromArgb(41, 133, 48);
            colors[2] = Color.FromArgb(52, 169, 61);
            colors[3] = Color.FromArgb(71, 199, 81);
            colors[4] = Color.FromArgb(101, 207, 109);
            colors[5] = Color.FromArgb(117, 213, 124);
            colors[6] = Color.FromArgb(156, 219, 111);
            colors[7] = Color.FromArgb(196, 225, 104);
            colors[8] = Color.FromArgb(231, 218, 99);
            colors[9] = Color.FromArgb(140, 111, 60);
            colors[10] = Color.FromArgb(127, 127, 127);
            colors[11] = Color.FromArgb(255, 255, 255);
        }

        public static void Draw(Map map, string fileNameBase)
        {
            var bmp = new Bitmap(map.Width, map.Height);
            var g = Graphics.FromImage(bmp);
            g.FillRectangle(Brushes.Red, 0, 0, map.Width, map.Height);

            map.ForEachCell(cell =>
            {
                Color color = cell.Height == 0
                    ? Color.FromArgb(179, 226, 238)
                    : colors[Convert.ToInt32(Math.Floor(cell.Height))];

                var brush = new SolidBrush(color);
                g.FillRectangle(brush, cell.X, cell.Y, 1, 1);
            });

            using (var stream = new MemoryStream())
            {
                bmp.Save(stream, ImageFormat.Jpeg);
                bmp.Save($"{fileNameBase}.jpg");
            }
        }
    }
}
