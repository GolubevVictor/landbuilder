﻿using LandBuilder.DomainModels;
using System;
using System.IO;

namespace LandBuilder.ConsoleApp.Utilities
{
    internal class FileDataAccess
    {
        internal static void Save(Map map, string path)
        {
            using (Stream stream = File.Open(path, FileMode.Create))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                binaryFormatter.Serialize(stream, map);
            }
        }

        internal static Map Read(string path = null)
        {
            while (!File.Exists(path))
            {
                Console.Write("Input file path: ");
                path = Console.ReadLine();
            }

            using (Stream stream = File.Open(path, FileMode.Open))
            {
                var binaryFormatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                return (Map)binaryFormatter.Deserialize(stream);
            }
        }
    }
}
