## Seamless map creation app.

Some time ago I wanted to create an online interplanetar strategy. So first step of strategy, you need a map. So, I've started from LandBuilder.

**Work is in progress.**

- Billet creation and filling with random height cells.
- Set relation of cells through borders
- Adding random non-linear mountain chains
- Adding continents
- Smooth map (calculating avarage height in specified area limited by diameter)
- Save/Load map billet for having possibility to tune landscape, rivers etc.
- Defining sea line.
- Create landscape for heights over sea line with predominance of planes. Landscape consists of 12 levels.
- Colorization of map and creation of an image of map in bmp format

**>>>Work is here<<<**

- Add rivers and lakes.
- Add life
- Add climat zones
