﻿using LandBuilder.DomainModels;
using LandBuilder.RandomHeightSmoothedMapGenerator.Models;
using System;

namespace LandBuilder.RandomHeightSmoothedMapGenerator.AdditionalBuilders
{
    internal class MountainBuilder
    {
        private const int ChainLength = 20;

        private static Random Random = new Random();

        private readonly Map _map;
        public MountainBuilder(Map map)
        {
            _map = map;
        }

        public bool Build(Cell start, Cell end)
        {
            double initDistance = _map.GetDistance(start, end);
            if (initDistance < 5 * ChainLength)
            {
                return false;
            }

            var currentCell = start;
            while (!currentCell.Equals(end))
            {
                double distanceToStart = _map.GetDistance(currentCell, start);
                double distanceToEnd = _map.GetDistance(currentCell, end);
                Degree finalDestinationDegree = GetDegree(currentCell, end);

                if (distanceToEnd > ChainLength)
                {
                    double random = (Random.NextDouble() - .5) * 2;
                    finalDestinationDegree.AddAngle(random);
                }

                (int offsetByX, int offsetByY) = currentCell.GetCoordinateOffcets(finalDestinationDegree.Radian, ChainLength);
                double agmentation = GetAgmentation(distanceToStart, distanceToEnd);

                // Select main oxis for drawing (along x or y)
                if (finalDestinationDegree.Radian <= Math.PI / 4)
                {
                    // coef consists of Bias Factor, and vartical and horisontal directions
                    //      Pure math
                    var coefY = GetBiasFactor(offsetByY, offsetByX) * (int)finalDestinationDegree.Vertical * (int)finalDestinationDegree.Horizontal;
                    int destinationCoordinate = offsetByX * (int)finalDestinationDegree.Horizontal;
                    for (int i = 0; i != destinationCoordinate; i += (int)finalDestinationDegree.Horizontal)
                    {
                        int currentX = currentCell.X + i;
                        int currentY = Convert.ToInt32(Math.Round(i * coefY + currentCell.Y));
                        _map[currentX, currentY].Height += agmentation;
                    }
                }
                else
                {
                    var coefX = GetBiasFactor(offsetByX, offsetByY) * (int)finalDestinationDegree.Vertical * (int)finalDestinationDegree.Horizontal;
                    int destinationCoordinate = offsetByY * (int)finalDestinationDegree.Vertical;
                    for (int i = 0; i != destinationCoordinate; i += (int)finalDestinationDegree.Vertical)
                    {
                        int currentX = Convert.ToInt32(Math.Round(i * coefX + currentCell.X));
                        int currentY = currentCell.Y + i;
                        _map[currentX, currentY].Height = agmentation;
                    }
                }
                currentCell = distanceToEnd < ChainLength
                    ? end
                    : _map[currentCell.X + offsetByX * (int)finalDestinationDegree.Horizontal, currentCell.Y + offsetByY * (int)finalDestinationDegree.Vertical];
            }

            return true;
        }

        private Degree GetDegree(Cell current, Cell end)
        {
            double hypotenuse = _map.GetDistance(current, end);
            double cathet = _map.GetDistanceByX(current, end);
            double cosine = cathet / hypotenuse;
            double angleValue = Math.Acos(cosine);

            return new Degree
            {
                Radian = angleValue,
                Horizontal = current.X <= end.X
                    ? Degree.HFlag.Right
                    : Degree.HFlag.Left,
                Vertical = end.Y < current.Y
                    ? Degree.VFlag.Up
                    : Degree.VFlag.Down
            };
        }

        private double GetBiasFactor(double param1, double param2)
        {
            return param2 != 0
                ? param1 / param2
                : param1;
        }

        private double GetAgmentation(double distanceToStart, double distanceToEnd)
        {
            if (distanceToEnd == 0 || distanceToStart == 0)
            {
                return 0;
            }

            double shortDistance, longDistance;
            if (distanceToEnd > distanceToStart)
            {
                shortDistance = distanceToStart;
                longDistance = distanceToEnd;
            }
            else
            {
                shortDistance = distanceToEnd;
                longDistance = distanceToStart;
            }

            var coef = shortDistance / longDistance;
            return Math.Sqrt(coef) / 100000;
        }
    }
}
