﻿using System;

namespace LandBuilder.RandomHeightSmoothedMapGenerator.Models
{
    internal class Degree
    {
        internal double Radian { get; set; }
        internal VFlag Vertical { get; set; }
        internal HFlag Horizontal { get; set; }

        internal Degree AddAngle(double delta)
        {
            if (delta > Math.PI / 2 || delta < -Math.PI / 2)
            {
                throw new ArgumentException("Degree abs delta cannot be more than 90");
            }

            Radian += delta;

            if (Radian < 0)
            {
                Radian = Math.Abs(Radian);
                Vertical = Vertical == VFlag.Up
                    ? VFlag.Down
                    : VFlag.Up;
            }
            else if (Radian > Math.PI / 2)
            {
                Radian = Math.PI - Radian;
                Horizontal = Horizontal == HFlag.Left
                    ? HFlag.Right
                    : HFlag.Left;
            }

            return this;
        }

        internal enum VFlag
        {
            Up = -1,
            Down = 1
        }

        internal enum HFlag
        {
            Left = -1,
            Right = 1
        }
    }
}
