﻿using LandBuilder.DomainModels;
using LandBuilder.RandomHeightSmoothedMapGenerator.AdditionalBuilders;
using System;
using System.Collections.Generic;

namespace LandBuilder.RandomHeightSmoothedMapGenerator
{
    public class MapGenerator
    {
        protected static Random Random = new Random();

        public Map Generate(int mapHeight, int smooth)
        {
            var map = new Map(mapHeight * 2, mapHeight);
            RandomizeHeighs(map);

            CreteContinents(map);

            Mountains(map);

            Equalize(map, smooth);
            return map;
        }

        private void RandomizeHeighs(Map map)
        {
            map.ForEachCell(c =>
            {
                c.Height = Random.NextDouble();
            });
        }

        private void Equalize(Map map, int smooth)
        {
            map.ForEachCell(cell =>
            {
                var heightSum = 0d;
                var cellsCount = 0;
                for (var y = cell.Y - smooth; y <= cell.Y + smooth; y++)
                {
                    for (var x = cell.X - smooth; x <= cell.X + smooth; x++)
                    {
                        if (map.GetDistance(cell, map[x, y]) < smooth)
                        {
                            heightSum += map[x, y].Height;
                            cellsCount++;
                        }
                    }
                }
                cell.CalculatedHeight = heightSum / cellsCount;
            });

            map.ForEachCell(cell =>
            {
                cell.Height = cell.CalculatedHeight;
                cell.CalculatedHeight = 0;
            });
        }

        private void CreteContinents(Map map)
        {
            int continentSmooth = map.Height / 2;
            for (var i = 0; i < Random.Next(2, 6); i++)
            {
                CreateSingleContinent(map, map.RandomCell, continentSmooth);
            }
        }

        private void CreateSingleContinent(Map map, Cell centralCell, int continentSmooth)
        {
            map.ForEachCell(x =>
            {
                double distance = map.GetDistance(x, centralCell);
                if (distance < continentSmooth)
                {
                    x.Height += (continentSmooth - distance) / 100000;
                }
            });
        }

        private void Mountains(Map map)
        {
            var mountainsBuilder = new MountainBuilder(map);

            for (var i = 0; i < Random.Next(5, 10); i++)
            {
                mountainsBuilder.Build(map.RandomCell, map.RandomCell);
            }
        }
    }
}
