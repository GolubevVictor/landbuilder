﻿using LandBuilder.DomainModels;
using System;
using System.Collections.Generic;

namespace LandBuilder.Landscape12Handler
{
    public class Landscape
    {
        public void CreateRelief(Map map)
        {
            List<double> heights = new List<double>();

            map.ForEachCell(x => heights.Add(x.Height));
            heights.Sort();
            var waterline = heights[heights.Count / 100 * 65];

            double highest = 0;
            double lowest = 1;

            map.ForEachCell(c =>
            {
                if (c.Height > highest) highest = c.Height;
                if (c.Height < lowest) lowest = c.Height;
            });

            // 12 levels of hight (0 - 11)
            double coef = (highest - waterline) / 11;

            Func<double, double> calculateHeight = height =>
            {
                double level = (height - waterline) / coef;
                return Math.Pow(level, 2) / 11;
            };

            map.ForEachCell(x =>
            {
                x.Height = x.Height > waterline
                    ? calculateHeight(x.Height)
                    : 0;
            });
        }
    }
}
